import serial
import time


def main():
    ser = serial.Serial("COM2")

    pwm = 0

    while(True):
        time.sleep(2)
        ser.write(b'1\n')
        temp = int(ser.readline().split()[0])
        print("temp: " + str(temp) + "\nEnter (1, 255) for motor pwm" +
              "\n or Enter 0 to automatic" + "\n or -1 for exit.")
        choice = int(input())
        if choice == -1:
            break
        if choice == 0:
            pwm = int((temp/100)*255)
        else:
            pwm = choice
        ser.write((str(pwm)+"\n").encode())


if __name__ == '__main__':
    main()
